clear

docker network create traefik --scope swarm

# docker network create --driver=weaveworks/net-plugin:latest_release --attachable weavenet
### traefik ###
cd traefik 
./run.sh 
cd ..
##############
### portainer ###
cd portainer 
./run.sh 
cd ..
##############
### backend ###
cd backend
./run.sh 
cd ..
##############
### krakend ###
cd krakend
./run.sh 
cd ..
##############
### whoami ###
cd whoami 
./run.sh
cd ..
##############
docker system prune -f
docker ps -a
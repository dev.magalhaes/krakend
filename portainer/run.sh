#docker compose
docker volume create portainer_data
#docker-compose up --remove-orphans -d portainer

#docker swarm
docker stack rm $(docker stack ls  | grep 'portainer' | awk '{print $1}')
docker stack deploy --compose-file docker-compose.yaml portainer
package main

import (
	"encoding/json"
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

type event struct {
	name string `json:"name"`
}

type allEvents []event

var events = allEvents{
	{
		name: "Lucas Felipe",
	},
}

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/users", createEvent)
	log.Fatal(http.ListenAndServe(":5001", router))
}

func createEvent(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(events)
}

#docker compose
#docker stop $(docker ps -a  | grep 'traefik' | awk '{print $1}')
#docker rm $(docker ps -a  | grep 'traefik' | awk '{print $1}')
#docker-compose up --remove-orphans -d traefik

#docker swarm
docker stack rm $(docker stack ls  | grep 'traefik' | awk '{print $1}')
docker stack deploy --compose-file docker-compose.yaml traefik
#docker compose
#docker stop $(docker ps -a  | grep 'krakend' | awk '{print $1}')
#docker rm $(docker ps -a  | grep 'krakend' | awk '{print $1}')
#docker-compose up --remove-orphans -d krakend

#docker swarm
docker stack rm $(docker stack ls  | grep 'krakend' | awk '{print $1}')
docker stack deploy --compose-file docker-compose.yaml krakend